import * as React from 'react';
import { useRouter } from 'next/router';
import Layout, { ComponentType } from '@components/Layout';
import Head from '@components/Head';
import List from '@components/_page/component/List';
import { BodySTY } from '@style/docs';
import Install from '@components/Mdx/install.mdx';
import Start from '@components/Mdx/start.mdx';

const MyH1 = (props) => <h1 style={{ color: 'tomato' }} {...props} />;

const components = {
    h1: MyH1,
};

const list = [
    { name: 'Start', dom: Start },
    { name: 'Install', dom: Install },
];

const Page: ComponentType = (props) => {
    const { setLoading, data } = props;

    const router = useRouter();
    const { name } = router.query;

    const [Com, setCom] = React.useState<JSX.Element>(null);

    // React.useEffect(() => {
    //     setLoading(false);
    // }, []);

    React.useEffect(() => {
        if (name) {
            const item = list.find((item) => item.name === name);
            if (item) {
                const Dom = item?.dom;
                setCom(<Dom components={components} />);
            }
        }
        setLoading(false);
    }, [router.query]);

    return (
        <BodySTY>
            <Head title={'Scutoid/React use Docs'} description={'Scutoid React Docs'} />
            <List value={list.map((item) => item.name)} page="docs" />
            <div>{!!Com && Com}</div>
        </BodySTY>
    );
};

export default Layout({})(Page);
