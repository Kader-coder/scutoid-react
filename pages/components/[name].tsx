import React from 'react';
import dynamic from 'next/dynamic';
import Layout, { ComponentType } from '@components/Layout';
import List from '@components/_page/component/List';
import Theme from '@components/_page/component/Theme';
import Card from '@components/_page/component';
import Head from '@components/Head';
import { BodySTY } from '@style/components/name';

const Page: ComponentType = (props) => {
    const { setLoading, data } = props;

    const list = Array.from(process.env.LIST);
    const [name, setName] = React.useState(data?.name);
    const [components, setComponents] = React.useState(null);

    React.useEffect(() => {
        components && setLoading(false);
    }, [components]);

    React.useEffect(() => {
        if (data) {
            setName(data?.name);
            setComponents(
                data?.components?.map((item) =>
                    dynamic(() =>
                        import(`@components/_Kader/${data?.name}`).then((mod) => {
                            return mod[item.name];
                        }),
                    ),
                ),
            );
        }
    }, [data]);

    // console.log(data,process.env);

    return (
        <BodySTY>
            <Theme keyValue={data?.name} />
            <Head
                title={data?.name}
                description={`Scutoid React Components ${data?.name}`}
                image={`/_next/image?url=/${data?.name}.png&w=32&q=100`}
            />
            <List value={list} />
            {components && data?.name === name && (
                <div>
                    <h1>{data?.name}</h1>
                    <h2>Examples</h2>
                    {data?.components?.map((item, index) => {
                        const DynamicComponent = components[index];

                        return (
                            <div key={item.name}>
                                <Card
                                    main={data?.name}
                                    code={data?.detal?.import.concat(item?.code?.split(/\n/gi))}
                                    title={item.name}
                                >
                                    <DynamicComponent />
                                </Card>
                            </div>
                        );
                    })}
                </div>
            )}
        </BodySTY>
    );
};

export const getServerSideProps = async (ctx) => {
    const Component = require(`@components/_Kader/${ctx.query.name}`);
    const data = process.env.Components[ctx.query.name];

    const regexp = /(export const (.|\n|\r)*};)/;

    const dataList = data
        .match(regexp)[0]
        .split(/export /gi)
        .filter((item) => item.length);
    // .map((data) => data.match(/(.|\n|\r)*\);\r\n};/)[0]);

    // const list = data.match(/export const (.)*/g);

    return {
        props: {
            name: ctx.query.name,
            components: Object.keys(Component)
                .filter((keyName) => keyName !== 'default')
                .map((name, index) => {
                    return {
                        name: name,
                        desc: Component[name]?.Desc || '',
                        code: dataList[index]?.replace(/(\r\n){2,}/gi, '\n').replace(/ /g, '\u00a0'),
                    };
                }),
            detal: {
                title: Component.default.title,
                import: data.match(/import(.)*/g).concat(''),
            },
        },
    };
};

export default Layout({})(Page);
