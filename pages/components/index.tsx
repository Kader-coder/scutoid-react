import React from 'react';
import Image from 'next/image';
import Link from 'next/link';
import Layout, { ComponentType } from '@components/Layout';
import List from '@components/_page/component/List';
import Head from '@components/Head';
import { BodySTY } from '@style/components';

const Page: ComponentType = (props) => {
    const { setLoading, data } = props;
    const list = Array.from(process.env.LIST);

    React.useEffect(() => {
        setLoading(false);
    }, []);

    return (
        <BodySTY>
            <Head
                title={'Scutoid/React example for components'}
                description={'Scutoid React Components'}
                image="/all.png"
            />
            <List value={list} />
            <div>
                <h1>Components</h1>
                <h2>Common</h2>
                <div className="list">
                    {list?.map((name, index) => {
                        return (
                            <Link
                                key={`${name}-${index}`}
                                href={{ pathname: '/components/[name]', query: { name: name } }}
                                passHref
                            >
                                <a>
                                    <h3>{name}</h3>
                                    <Image
                                        src={`/components/${name}.png`}
                                        width={'auto'}
                                        height={'auto'}
                                        quality={87}
                                    ></Image>
                                    {/* <article>name</article> */}
                                </a>
                            </Link>
                        );
                    })}
                </div>
            </div>
        </BodySTY>
    );
};

export default Layout({})(Page);
