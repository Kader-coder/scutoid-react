import * as React from 'react';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import Head from 'next/head';
import type { AppProps } from 'next/app';
import { ThemeProvider } from 'styled-components';
import theme from '@style/theme';
import GlobalSTY from '@style/global';
import todoApp from '@root/reducers';
import { GridSTY } from '@style/grid';
import Header from '@components/Layout/Header';
import Footer from '@components/Layout/Footer';

const store = createStore(todoApp);

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>;
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch;

const MyApp = (props: AppProps) => {
    const { Component, pageProps } = props;
    const [theme, setTheme] = React.useState(store.getState().theme);

    // console.log(store.getState().theme);

    return (
        <Provider store={store}>
            <React.Fragment>
                <GlobalSTY />
                <ThemeProvider theme={theme}>
                    <Head>
                        <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
                        <meta
                            name="viewport"
                            content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no"
                        />
                        <meta name="keywords" content="Keywords" />

                        <link rel="manifest" href="/manifest.json" />
                        <meta name="theme-color" content="#211E55" />

                        {/* iOS */}
                        <link rel="apple-touch-icon" href="/icon/256x256.png"></link>
                        <meta name="apple-mobile-web-app-title" content="Application Title" />
                        <meta name="apple-mobile-web-app-capable" content="yes" />
                        <meta name="apple-mobile-web-app-status-bar-style" content="default" />
                    </Head>

                    <GridSTY rows={1} gridRows={'1fr'} style={{ minHeight: '100vh' }}>
                        <Header />
                        <Component {...pageProps} setTheme={setTheme} />
                        <Footer />
                    </GridSTY>
                </ThemeProvider>
            </React.Fragment>
        </Provider>
    );
};

export default MyApp;
