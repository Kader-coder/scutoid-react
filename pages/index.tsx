import * as React from 'react';
import Link from 'next/link';
import { useRouter } from 'next/router';
import Layout, { ComponentType } from '@components/Layout';
import Head from '@components/Head';
import { Button } from '@scutoid/react';
import { BodySTY, CoverSTY, ReadSTY } from '@style/index/style';

const FurtherReading = [
    {
        name: 'Typescript',
        description: 'TypeScript extends JavaScript by adding types to the language',
        img: 'https://www.typescriptlang.org/images/index/ts-conf-keynote.jpg',
        herf: 'https://www.typescriptlang.org/',
    },
    {
        name: 'Styled-components',
        description:
            'Visual primitives for the component age. Use the best bits of ES6 and CSS to style your apps without stress ',
        img: 'https://ms314006.github.io/static/a5aeda9f015ec69a5fcb46513d6ec7a7/22c48/styled-components.jpg',
        herf: 'https://styled-components.com/',
    },
    {
        name: 'Next.js',
        description: 'Production grade React applications that scale',
        img: 'https://nextjs.org/static/twitter-cards/home.jpg',
        herf: 'https://nextjs.org/',
    },
];

const Page: ComponentType = (props) => {
    const { setLoading } = props;
    const router = useRouter();

    React.useEffect(() => {
        setLoading(false);
        // router.push('/components');
    }, []);
    // console.log(process.env.DEBUG);

    return (
        <BodySTY>
            <Head
                title="Scutoid/React for Developer in React"
                description="Scutoid-react provide you all the components need for production，TypeScript support,Style support
                    ,And can be customized, Don`t have to Webpack`s config."
                image="/icon/256x256.png"
            />
            <CoverSTY>
                <h1>ScutoidReact provide simple and fast for React Components</h1>
                <h2>
                    Scutoid-react provide you all the components need for production，TypeScript support,Style support
                    ,And can be customized, Don`t have to Webpack`s config.
                </h2>
                <Button>
                    <Link href={{ pathname: '/components' }} passHref>
                        <a>Go use →</a>
                    </Link>
                </Button>
                <a href={process.env.NPM_HERF}>npm</a>
            </CoverSTY>
            <ReadSTY>
                <h2>Further Reading</h2>
                {FurtherReading.map((item) => (
                    <a
                        key={`Index-Read-${item.name}`}
                        style={{ backgroundImage: `url('${item.img}')` }}
                        data-hover={item.description}
                        href={item.herf}
                    >
                        <div>{item.name}</div>
                    </a>
                ))}
            </ReadSTY>
        </BodySTY>
    );
};

// export const getServerSideProps = async (ctx) => {
//     // let data: any = null;

//     return {
//         props: {
//         },
//     };
// };

export default Layout({})(Page);
