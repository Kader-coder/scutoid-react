import styled from 'styled-components';

interface I_GridSTY {
    columnsContent?: string;
    rowsContent?: string;
    gridColumns?: string;
    columns?: number;
    gridRows?: string;
    rows?: number;
    gap?: number;
    noRow?: boolean;
}
const _repeat = (count?: number, content?: string) => {
    const Content = content || 'auto';
    return count ? `repeat(${count}, ${Content})` : ``;
};

export const GridSTY = styled.div<I_GridSTY>`
    display: grid;
    grid-template-columns: ${(props) => _repeat(props.columns, props.columnsContent)} ${(props) => props.gridColumns};
    grid-template-rows: ${(props) => _repeat(props?.rows, props.rowsContent)} ${(props) => props.gridRows};
    grid-gap: ${(props) => props.gap || 0}px;
    grid-auto-flow: ${({ noRow }) => (noRow ? 'column' : 'auto')};
    width: 100%;
`;

export default {};
