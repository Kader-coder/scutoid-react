import styled from 'styled-components';

export const BodySTY = styled.div`
    display: grid;
    grid-gap: 36px;
    grid-template-columns: auto;

    > div {
        display: grid;
        grid-gap: 20px;
        height: fit-content;
    }

    @media screen and (min-width: ${({ theme }) => theme.screen.tablet}) {
        grid-template-columns: auto 1fr;
    }
`;

export default {};
