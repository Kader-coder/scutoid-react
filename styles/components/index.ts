import styled from 'styled-components';

export const BodySTY = styled.div`
    display: grid;
    grid-template-columns: auto;
    grid-gap: 28px;

    > div {
        display: grid;
        grid-gap: 20px;
        height: fit-content;

        > div.list {
            display: grid;
            grid-template-columns: repeat(2, 1fr);
            grid-gap: 8px;
            max-width: 720px;
            > a {
                text-align: center;
                padding: 0px 8px;
                border: solid 1px transparent;
                transition: all 0.32s;
                cursor: pointer;

                &:hover {
                    border-radius: 4px;
                    border-color: #ddd;
                    /* transform: scale(1.05); */
                    box-shadow: 0px 2px 4px -1px rgba(0, 0, 0, 0.2), 0px 4px 5px 0px rgba(0, 0, 0, 0.14),
                        0px 1px 10px 0px rgba(0, 0, 0, 0.12);
                }
                > div {
                }

                img {
                    object-fit: contain;
                    height: 128px;
                    z-index: -1;
                }
            }
        }
    }

    @media screen and (min-width: ${({ theme }) => theme.screen.tablet}) {
        > div {
            padding-right: 4px;
            padding-bottom: 8px;

            > div.list {
                grid-gap: 16px 32px;
                grid-template-columns: repeat(3, 1fr);
                max-width: 960px;
            }
        }
    }

    @media screen and (min-width: ${({ theme }) => theme.screen.tablet}) {
        grid-template-columns: auto 1fr;
    }

    @media screen and (min-width: ${({ theme }) => theme.screen.desktop}) {
        > div {
            > div.list {
                grid-template-columns: repeat(4, 1fr);
                max-width: 1260px;
            }
        }
    }
`;

export default {};
