import styled, { createGlobalStyle } from 'styled-components';

export const GlobalSTY = createGlobalStyle`
    body {
        overflow-y: overlay;
        /* background-color: #828282; */
    }

    * {
        font-family: Microsoft JhengHei;
        box-sizing: border-box;
    }
    
 
    a {
        color: inherit;
        text-decoration: none;
    }

    pre {
        background: #f5f5f5;
        margin: 16px 0;
        padding: 12px 20px;
        overflow: auto;
    }

    .breadcrumb {
        font-size: 14px;
        margin: 8px 0;
    }


    .bold{
        font-weight:bold;
    }
    ::-webkit-scrollbar {
        width: 5px;
    }

    ::-webkit-scrollbar-track {
        -webkit-border-radius: 10px;
        border-radius: 10px;
        background: transparent;
    }

    ::-webkit-scrollbar-thumb {
        -webkit-border-radius: 4px;
        border-radius: 4px;
        background: #0005;
    }

`;

export const ContainerSTY = styled.div`
    display: block;
    max-width: ${({ theme }) => theme.screen.desktop};
    width: 100%;
    padding: 0 12px;
    margin: 0 auto;
    box-sizing: border-box;
`;

export default GlobalSTY;
