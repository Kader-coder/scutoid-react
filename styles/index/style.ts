import styled from 'styled-components';

export const BodySTY = styled.div`
    display: grid;
    grid-gap: 12px;

    > div {
        display: grid;
    }

    @media screen and (min-width: ${({ theme }) => theme.screen.laptop}) {
    }
`;

export const CoverSTY = styled.div`
    justify-items: center;

    h1,
    h2 {
        text-align: center;
    }

    h1 {
        margin-bottom: 0px;
        font-size: 24px;
        font-weight: bold;
    }

    h2 {
        margin-bottom: 40px;
        font-size: 16px;
        color: #bbb;
    }

    > a {
        margin-top: 8px;
        color: ${({ theme }) => theme.DefColor.OUCHI};
    }

    @media screen and (min-width: ${({ theme }) => theme.screen.laptop}) {
        h1 {
            font-size: 64px;
        }

        h2 {
            font-size: 20px;
        }
    }
`;

export const ReadSTY = styled.div`
    grid-gap: 12px;

    h2 {
        grid-column: 1/-1;
        font-size: 22px;
        font-weight: bold;
    }

    > a {
        position: relative;
        height: 200px;
        letter-spacing: 0.02em;
        color: #fff;
        font-weight: bold;
        background-color: #cccccc;
        background-position: center;
        background-repeat: no-repeat;
        background-size: cover;
        position: relative;
        overflow: hidden;
        font-size: 18px;

        > div {
            position: absolute;
            width: 100%;
            padding: 8px 12px;
            background-color: #0003;
            z-index: 1;
        }

        &::after {
            content: attr(data-hover);
            position: absolute;
            height: 100%;
            width: 100%;
            top: 100%;
            background-color: #0006;
            transition: top 0.16s ease;
            display: flex;
            align-items: flex-end;
            padding: 8px 12px;
            box-sizing: border-box;
            z-index: 0;
        }

        &:hover::after {
            top: 0%;
        }
    }

    @media screen and (min-width: ${({ theme }) => theme.screen.laptop}) {
        grid-template-columns: repeat(3, 1fr);

        h2 {
            font-size: 22px;
        }
    }

    @media screen and (min-width: ${({ theme }) => theme.screen.desktop}) {
        > a {
            height: 240px;
        }
    }
`;
export default {};
