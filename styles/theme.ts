export const DefColor = {
    MESSHI: '#533D5B', //
    KONKIKYO: '#211E55', //
    KIKYO: '#6A4C9C', //
    OUCHI: '#9B90C2',

    GOFUN: '#FFFFFB',
    SHIRONERI: '#FCFAF2',
    SHIRONEZUMI: '#BDC0BA',
    HAI: '#828282',
    NAMARI: '#787878',
    KESHIZUMI: '#434343',
    SUMI: '#1C1C1C',
    RO: '#0C0C0C',
    KURO: '#080808',
    // 黑白
};

const theme = {
    screen: {
        phone: '480px', // 手機
        tablet: '980px', // 平板
        laptop: '1280px', // 筆電、桌機
        desktop: '1440px', // 較大桌機
    },

    DefColor: DefColor,

    font: {
        S: '12px',
        M: '14px',
        L: '16px',
        XL: '18px',
        XXL: '20px',
        X3L: '22px',
        X4L: '24px',
        X5L: '26px',
        E: '28px',
        ES: '30px',
        EM: '32px',
        EL: '34px',
        EX: '36px',
    },

    Scutoid: {
        Badge: {
            Color: '#fff',
            BackgroundColor: '#f5222d',
            FontSize: 12,
        },
        Button: {
            PrimaryColor: '#03a9f4',
            Color: '#666',
            BorderColor: '#ddd',
        },
        Card: {
            secondary: DefColor.OUCHI,
        },
        Input: {
            PrimaryColor: '#03a9f4',
            FontColor: '#666',
            InputColor: 'rgba(0, 0, 0, 0.87)',
            BorderColor: '#666',
            ErrorColor: '#b00020',
        },
        Pagination: {
            PrimaryColor: '#03a9f4',
            BackgroundColor: '#fff',
            BorderColor: '#666',
        },
        Progress: {
            PrimaryColor: '#03a9f4',
            BackgroundColor: '#bdbdbd',
            ErrorColor: '#b00020',
            SucceedColor: '#86c166',
            FontColor: '#666',
        },
        Select: {
            PrimaryColor: '#03a9f4',
            BackgroundColor: '#fff',
            BorderColor: '#d9d9d9',
        },
        Table: {
            PrimaryColor: '#03a9f4',
            BorderColor: '#f0f0f0',
            BackgroundColor: '#f8f8f8',
            TitleColor: '#f8f8f8',
            FontColor: 'rgba(0, 0, 0, 0.87)',
        },
        Tab: {
            PrimaryColor: '#ff0000',
            Color: '#666666',
            BackgroundColor: '#ddd9',
        },
        Tag: {
            Color: '#000',
            BackgroundColor: 'transparent',
        },
        Radio: {
            PrimaryColor: '#03a9f4',
            DisabledColor: '#aaa',
        },
        Checkbox: {
            PrimaryColor: '#03a9f4',
            DisabledColor: '#aaa',
        },
        Divider: {
            Color: '#666',
            BorderColor: '#d9d9d9',
        },
    },
};

export default theme;
