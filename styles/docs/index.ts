import styled from 'styled-components';

export const BodySTY = styled.div`
    display: grid;
    grid-gap: 36px;
    grid-template-columns: auto;

    /* h2 {
        margin-bottom: 8px;
    } */

    /* p {
        margin: 4px 0;
    } */
    @media screen and (min-width: ${({ theme }) => theme.screen.tablet}) {
        grid-template-columns: auto 1fr;
    }
`;

export default {};
