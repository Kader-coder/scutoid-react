require('dotenv').config();
const path = require('path');
const { merge } = require('webpack-merge');
const withCSS = require('@zeit/next-css');
const withMDX = require('@next/mdx')();
const withPWA = require('next-pwa');
const root = path.resolve('./');
const common = require(path.resolve(root, 'config/webpack.commons.ts'));

module.exports = withMDX();

module.exports = withCSS(
    withPWA({
        pwa: {
            dest: 'public',
            disable: process.env.DEBUG === 'true',
            register: true,
        },
        webpack(config, options) {
            const mergeConfig = merge(config, common);

            mergeConfig.module.rules.push({
                test: /\.(png|jpg|gif|svg|eot|ttf|woff|woff2)$/,
                use: {
                    loader: 'url-loader',
                    options: {
                        limit: 100000,
                    },
                },
            });

            mergeConfig.module.rules.push({
                test: /\.svg$/,
                use: [
                    {
                        loader: '@svgr/webpack',
                    },
                ],
            });

            mergeConfig.module.rules.push({
                test: /\.mdx$/,
                use: [
                    options.defaultLoaders.babel,
                    {
                        loader: require.resolve('@mdx-js/loader'),
                        options: options,
                    },
                ],
            });

            return mergeConfig;
        },
        generateInDevMode: false,
    }),
);
