import * as React from 'react';

const SvgLoding = (props: React.SVGProps<SVGSVGElement>) => (
    <svg width="1em" height="1em" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" display="block" {...props}>
        <rect x={48.5} y={25} rx={1.5} ry={2.2} width={3} height={10} fill="#999">
            <animate
                attributeName="opacity"
                values="1;0"
                keyTimes="0;1"
                dur="1s"
                begin="-0.9411764705882353s"
                repeatCount="indefinite"
            />
        </rect>
        <rect x={48.5} y={25} rx={1.5} ry={2.2} width={3} height={10} fill="#999" transform="rotate(21.176 50 50)">
            <animate
                attributeName="opacity"
                values="1;0"
                keyTimes="0;1"
                dur="1s"
                begin="-0.8823529411764706s"
                repeatCount="indefinite"
            />
        </rect>
        <rect x={48.5} y={25} rx={1.5} ry={2.2} width={3} height={10} fill="#999" transform="rotate(42.353 50 50)">
            <animate
                attributeName="opacity"
                values="1;0"
                keyTimes="0;1"
                dur="1s"
                begin="-0.8235294117647058s"
                repeatCount="indefinite"
            />
        </rect>
        <rect x={48.5} y={25} rx={1.5} ry={2.2} width={3} height={10} fill="#999" transform="rotate(63.53 50 50)">
            <animate
                attributeName="opacity"
                values="1;0"
                keyTimes="0;1"
                dur="1s"
                begin="-0.7647058823529411s"
                repeatCount="indefinite"
            />
        </rect>
        <rect x={48.5} y={25} rx={1.5} ry={2.2} width={3} height={10} fill="#999" transform="rotate(84.706 50 50)">
            <animate
                attributeName="opacity"
                values="1;0"
                keyTimes="0;1"
                dur="1s"
                begin="-0.7058823529411765s"
                repeatCount="indefinite"
            />
        </rect>
        <rect x={48.5} y={25} rx={1.5} ry={2.2} width={3} height={10} fill="#999" transform="rotate(105.882 50 50)">
            <animate
                attributeName="opacity"
                values="1;0"
                keyTimes="0;1"
                dur="1s"
                begin="-0.6470588235294118s"
                repeatCount="indefinite"
            />
        </rect>
        <rect x={48.5} y={25} rx={1.5} ry={2.2} width={3} height={10} fill="#999" transform="rotate(127.059 50 50)">
            <animate
                attributeName="opacity"
                values="1;0"
                keyTimes="0;1"
                dur="1s"
                begin="-0.5882352941176471s"
                repeatCount="indefinite"
            />
        </rect>
        <rect x={48.5} y={25} rx={1.5} ry={2.2} width={3} height={10} fill="#999" transform="rotate(148.235 50 50)">
            <animate
                attributeName="opacity"
                values="1;0"
                keyTimes="0;1"
                dur="1s"
                begin="-0.5294117647058824s"
                repeatCount="indefinite"
            />
        </rect>
        <rect x={48.5} y={25} rx={1.5} ry={2.2} width={3} height={10} fill="#999" transform="rotate(169.412 50 50)">
            <animate
                attributeName="opacity"
                values="1;0"
                keyTimes="0;1"
                dur="1s"
                begin="-0.47058823529411764s"
                repeatCount="indefinite"
            />
        </rect>
        <rect x={48.5} y={25} rx={1.5} ry={2.2} width={3} height={10} fill="#999" transform="rotate(190.588 50 50)">
            <animate
                attributeName="opacity"
                values="1;0"
                keyTimes="0;1"
                dur="1s"
                begin="-0.4117647058823529s"
                repeatCount="indefinite"
            />
        </rect>
        <rect x={48.5} y={25} rx={1.5} ry={2.2} width={3} height={10} fill="#999" transform="rotate(211.765 50 50)">
            <animate
                attributeName="opacity"
                values="1;0"
                keyTimes="0;1"
                dur="1s"
                begin="-0.35294117647058826s"
                repeatCount="indefinite"
            />
        </rect>
        <rect x={48.5} y={25} rx={1.5} ry={2.2} width={3} height={10} fill="#999" transform="rotate(232.941 50 50)">
            <animate
                attributeName="opacity"
                values="1;0"
                keyTimes="0;1"
                dur="1s"
                begin="-0.29411764705882354s"
                repeatCount="indefinite"
            />
        </rect>
        <rect x={48.5} y={25} rx={1.5} ry={2.2} width={3} height={10} fill="#999" transform="rotate(254.118 50 50)">
            <animate
                attributeName="opacity"
                values="1;0"
                keyTimes="0;1"
                dur="1s"
                begin="-0.23529411764705882s"
                repeatCount="indefinite"
            />
        </rect>
        <rect x={48.5} y={25} rx={1.5} ry={2.2} width={3} height={10} fill="#999" transform="rotate(275.294 50 50)">
            <animate
                attributeName="opacity"
                values="1;0"
                keyTimes="0;1"
                dur="1s"
                begin="-0.17647058823529413s"
                repeatCount="indefinite"
            />
        </rect>
        <rect x={48.5} y={25} rx={1.5} ry={2.2} width={3} height={10} fill="#999" transform="rotate(296.47 50 50)">
            <animate
                attributeName="opacity"
                values="1;0"
                keyTimes="0;1"
                dur="1s"
                begin="-0.11764705882352941s"
                repeatCount="indefinite"
            />
        </rect>
        <rect x={48.5} y={25} rx={1.5} ry={2.2} width={3} height={10} fill="#999" transform="rotate(317.647 50 50)">
            <animate
                attributeName="opacity"
                values="1;0"
                keyTimes="0;1"
                dur="1s"
                begin="-0.058823529411764705s"
                repeatCount="indefinite"
            />
        </rect>
        <rect x={48.5} y={25} rx={1.5} ry={2.2} width={3} height={10} fill="#999" transform="rotate(338.824 50 50)">
            <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="0s" repeatCount="indefinite" />
        </rect>
    </svg>
);

export default SvgLoding;
