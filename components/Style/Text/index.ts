import styled from 'styled-components';

export const NoDataSTY = styled.p`
    font-size: 22px;
    color: #bbb;
    font-weight: bold;
    text-align: center;
    padding-top: 40px;
    letter-spacing: 4px;

    @media screen and (min-width: ${({ theme }) => theme.screen.laptop}) {
        font-size: 30px;
    }
`;
