import React from 'react';
import cx from 'classnames';
import { BodySTY } from './style';

export interface MeunProps extends React.DetailedHTMLProps<React.HTMLAttributes<HTMLUListElement>, HTMLUListElement> {
    value?: boolean;
    ref?: React.MutableRefObject<HTMLUListElement>;
}

const Meun: React.FC<MeunProps> = (props) => {
    const { value, children, ...Div } = props;

    return (
        <BodySTY className={cx('Meun', { open: value })} {...Div}>
            <li></li>
            <li></li>
            <li></li>
        </BodySTY>
    );
};

export default Meun;
