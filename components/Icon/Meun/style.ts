import styled from 'styled-components';

export const BodySTY = styled.ul`
    display: grid;
    box-sizing: content-box;
    align-items: center;
    height: 24px;
    width: 28px;
    margin: 0;
    padding: 8px;
    cursor: pointer;
    user-select: none;

    li {
        height: 2px;
        background: #000;
        list-style: none;
        pointer-events: none;
        transition: 0.26s;

        &:nth-child(1),
        &:nth-child(3) {
            transform: translateY(0px) rotate(0deg);
        }
        li:nth-child(2) {
            opacity: 1;
        }
    }

    &.open li {
        &:nth-child(1) {
            transform: translateY(8px) rotate(-45deg);
        }
        &:nth-child(3) {
            transform: translateY(-8px) rotate(45deg);
        }
        &:nth-child(2) {
            opacity: 0;
        }
    }

    @media screen and (min-width: ${({ theme }) => theme.screen.tablet}) {
        display: none;
    }
`;

export default {};
