import React from 'react';
import cx from 'classnames';
import { Icon } from '@scutoid/react';
import { LoadingSTY } from './style';
import LodingSVG from '@public/svg/Loding';

interface T_Loading {
    absolute?: boolean;
    style?: React.CSSProperties;
}
const Loading: React.FC<T_Loading> = (props) => {
    const { absolute, style } = props;

    return (
        <LoadingSTY className={cx('Loading', { absolute: absolute })}>
            <Icon component={LodingSVG} style={style} />
        </LoadingSTY>
    );
};

export default Loading;
