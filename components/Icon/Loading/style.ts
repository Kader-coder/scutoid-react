import styled, { css } from 'styled-components';

export const LoadingCSS = css`
    > *:not(.Loading) {
        opacity: 0;
    }
`;

export const LoadingSTY = styled.div`
    &.absolute {
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
    }

    > div.xin-icon {
        display: flex;
        justify-content: center;
        font-size: 40px;
    }
`;

export default {};
