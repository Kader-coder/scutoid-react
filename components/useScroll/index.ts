import * as React from 'react';

export type ScrollDirection = 'up' | 'down' | 'init';

export interface UseScrollType {
    direction: ScrollDirection;
    window: Window;
}

let HeaderScrollY = 0;

const useScroll = (): UseScrollType => {
    const [status, setStatus] = React.useState<UseScrollType>({ direction: 'init', window: null });

    const _Scroll = () => {
        const screenScrollY = window.scrollY;
        if (screenScrollY < HeaderScrollY) {
            setStatus({ direction: 'up', window: window });
        } else {
            setStatus({ direction: 'down', window: window });
        }
        HeaderScrollY = screenScrollY;
    };

    React.useEffect(() => {
        document.addEventListener('scroll', _Scroll);
        return () => {
            document.removeEventListener('scroll', _Scroll);
        };
    }, []);

    return status;
};

export default useScroll;
