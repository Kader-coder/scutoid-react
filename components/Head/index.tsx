import React from 'react';
import Head from 'next/head';

export interface HeadType {
    title?: string;
    type?: 'article';
    description?: string;
    image?: string;
}

const Header: React.FC<HeadType> = (props) => {
    const { title, type, description, image, children } = props;

    return (
        <Head>
            {children}
            {title && (
                <React.Fragment>
                    <title>{title}</title>
                    <meta property="og:title" content={title} />
                </React.Fragment>
            )}
            {type && (
                <React.Fragment>
                    <meta name="type" content={type} />
                    <meta property="og:type" content={type} />
                </React.Fragment>
            )}
            {description && (
                <React.Fragment>
                    <meta name="description" content={description} />
                    <meta property="og:description" content={description} />
                </React.Fragment>
            )}
            {image && (
                <React.Fragment>
                    <meta name="image" content={image} />
                    <meta property="og:image" content={image} />
                </React.Fragment>
            )}
        </Head>
    );
};

Header.defaultProps = {
    type: 'article',
};

export default Header;
