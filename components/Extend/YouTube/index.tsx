import React from 'react';
import { BodySTY } from './style';

interface R_Category {
    iframe: React.DetailedHTMLProps<React.IframeHTMLAttributes<HTMLIFrameElement>, HTMLIFrameElement>;
    ratio?: number;

    mute?: boolean; //靜音 1
    autoplay?: boolean; //視頻自動開始播放 0
    loop?: boolean; //循環 0
    controls?: boolean; //顯示播放器控件 1
}

const _stringToRegex = (v: string) => {
    return v.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
};

export const _findRegex = (v: string): RegExp => {
    const rex = RegExp(_stringToRegex(v) + '', 'g');
    return rex;
};

const Module: React.FC<R_Category> = (props) => {
    const { ratio, iframe, ...set } = props;

    const _set = () => {
        const data = iframe;

        if (!_findRegex('?').test(data.src)) {
            data.src += '?';
        }

        Object.keys(set)?.forEach((key) => {
            if (!_findRegex(key).test(data.src)) {
                data.src += `&${key}=${set[key] ? 1 : 0}`;
            }
        });
        return data;
    };

    return (
        <BodySTY ratio={ratio}>
            <iframe {..._set()} />
        </BodySTY>
    );
};

Module.defaultProps = {
    ratio: 16 / 9,
};

export default Module;
