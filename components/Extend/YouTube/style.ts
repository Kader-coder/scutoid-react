import styled from 'styled-components';

export const BodySTY = styled.div<{ ratio: number }>`
    position: relative;
    padding: calc(100% / ${({ ratio }) => ratio} / 2) 0;
    overflow: hidden;

    > iframe {
        position: absolute;
        width: 100%;
        height: 100%;
        top: 0;
        left: 0;
    }

    @media screen and (min-width: ${({ theme }) => theme.screen.laptop}) {
    }
`;
export default {};
