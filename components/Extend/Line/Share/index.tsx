import React from 'react';
import { useScript } from '@util/useScript';
import { BodySTY } from './style';

declare global {
    interface Window {
        LineIt: any;
    }
}

interface FacebookType {
    defHref?: string;
}

const LineShare: React.FC<FacebookType> = (props) => {
    const { defHref } = props;

    const [state, setstate] = React.useState<JSX.Element>(null);
    const status = useScript('https://d.line-scdn.net/r/web/social-plugin/js/thirdparty/loader.min.js');

    React.useEffect(() => {
        if (status === 'ready') {
            window.LineIt.loadButton();
        }
    }, [status]);

    React.useEffect(() => {
        let href = defHref;
        if (!href && typeof window !== 'undefined') {
            href = window.location.href;
        }
        setstate(
            <div
                className="line-it-button"
                data-lang="zh_Hant"
                data-type="share-a"
                data-ver="3"
                data-url={href}
                data-color="default"
                data-size="small"
                data-count="false"
                // style={{ display: 'none' }}
            ></div>,
        );
    }, []);

    return <BodySTY className="LineShare">{state}</BodySTY>;
};

LineShare.defaultProps = {};

export default LineShare;
