import React from 'react';
// import { useScript } from '@util/useScript';
import { BodySTY } from './style';

interface FacebookType {
    defHref?: string;
}

const FacebookShare: React.FC<FacebookType> = (props) => {
    const { defHref } = props;
    // const status = useScript('https://connect.facebook.net/zh_TW/sdk.js#xfbml=1&version=v9.0');
    const [state, setstate] = React.useState<JSX.Element>(null);

    React.useEffect(() => {
        let href = defHref;
        if (!href && typeof window !== 'undefined') {
            href = window.location.href;
        }
        setstate(
            <iframe
                src={`https://www.facebook.com/plugins/like.php?href=${encodeURIComponent(
                    href,
                )}&width=110&layout=button_count&action=like&size=small&share=true&height=46&appId`}
                width={'auto'}
                height={'20px'}
                style={{ border: 'none', overflow: 'hidden' }}
                scrolling="no"
                frameBorder="0"
                allowFullScreen={true}
                allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"
            ></iframe>,
        );
    }, []);

    return <BodySTY className="FacebookShare">{state}</BodySTY>;
};

FacebookShare.defaultProps = {};

export default FacebookShare;
