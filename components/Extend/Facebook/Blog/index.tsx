import React from 'react';
// import { useScript } from '@util/useScript';
import { BodySTY } from './style';

interface FacebookType {
    defHref?: string;
}

const FacebookBlog: React.FC<FacebookType> = (props) => {
    const { defHref } = props;

    const [state, setstate] = React.useState<JSX.Element>(null);
    const ref = React.useRef<HTMLDivElement>(null);

    React.useEffect(() => {
        let href = defHref;
        if (!href && typeof window !== 'undefined') {
            href = window.location.href;
        }
        setstate(
            <iframe
                src={`https://www.facebook.com/plugins/page.php?href=${encodeURIComponent(defHref)}&tabs=timeline&width=${
                    ref.current?.clientWidth
                }px&height=480px&small_header=true&adapt_container_width=true&hide_cover=false&show_facepile=false&appId`}
                style={{ border: 'none', overflow: 'hidden' }}
                width="100%"
                height="480px"
                // scrolling="no"
                frameBorder="0"
                allowFullScreen={true}
                allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"
            ></iframe>,
        );
    }, [defHref]);

    // const status = useScript('https://connect.facebook.net/zh_TW/sdk.js#xfbml=1&version=v8.0');

    return (
        <BodySTY className="FacebookBlog" ref={ref}>
            {state}
        </BodySTY>
    );
};

FacebookBlog.defaultProps = {
    defHref: 'https://zh-tw.facebook.com/Xinmedia',
};

export default FacebookBlog;
