import React, { ReactHTML } from 'react';
import cx from 'classnames';
import { Divider } from '@scutoid/react';
import { BodySTY, ToolSTY } from './style';
import ListColor from './color';

interface ComponentProp {
    main: string;
    code: string[];
    title: string;
    desc?: string;
}

const SSS: React.FC<ComponentProp> = (props) => {
    const { code, main, title, desc, children } = props;
    const [hide, setHide] = React.useState(true);
    return (
        <BodySTY>
            <div className="children">{children}</div>
            <Divider orientation="left" reserve="24px">
                <h3>{title || 'No Nmae'}</h3>
            </Divider>
            <article>{'No Description'}</article>
            <ToolSTY>
                <div>
                    <button onClick={() => setHide((pre) => !pre)}>{`< ${hide ? 'Code' : 'Hide'} >`}</button>
                </div>
            </ToolSTY>
            <section className={cx('Code', { hide: hide })} title="Code">
                {code.map((item, index) => (
                    <p
                        key={`p-${index}`}
                        dangerouslySetInnerHTML={{
                            __html: item.replace(/[a-zA-Z0-9'"@-_]*/g, (match) => {
                                let className = '';
                                const data = ListColor(main);
                                Object.keys(data).some((key) => {
                                    if (
                                        data[key].some((n) => {
                                            if (n instanceof RegExp) {
                                                return n.test(match);
                                            } else {
                                                return n === match;
                                            }
                                        })
                                    ) {
                                        className = key;
                                        return true;
                                    }
                                });
                                return `<span class="${className}">${match}</span>`;
                            }),
                        }}
                    />
                ))}
            </section>
        </BodySTY>
    );
};

// Module.defaultProps = {

// };

export default SSS;
