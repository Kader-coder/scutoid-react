import styled from 'styled-components';

export const BodySTY = styled.aside`
    position: fixed;
    right: 4px;
    bottom: 20px;
    display: grid;
    grid-gap: 8px;
    border-radius: 100%;
    overflow: hidden;
    z-index: 12;
    cursor: pointer;
    user-select: none;

    > .Icon {
        font-size: 18px;
        padding: 16px;
        background-color: cadetblue;
        color: aliceblue;
        &:hover {
            filter: brightness(0.8);
        }
    }

    @media screen and (min-width: ${({ theme }) => theme.screen.tablet}) {
        right: 8px;

        > .Icon {
            font-size: 24px;
            padding: 20px;
        }
    }
`;

export const ListSTY = styled.aside`
    display: grid;
    grid-gap: 16px;
    /* overflow-y: auto; */
    width: 300px;

    .Trigger {
        width: 100%;
        cursor: pointer;
        user-select: none;

        .piker {
            display: grid;
            grid-gap: 4px;

            article {
                display: flex;
                align-items: center;
                column-gap: 8px;
                border-bottom: 1px solid transparent;

                &:hover {
                    border-color: #999;
                }

                sub {
                    width: 14px;
                    height: 14px;
                    padding: 2px;
                    border: 1px solid #dddd;
                    border-radius: 2px;
                }
            }
        }
    }

    .Window {
        background-color: transparent;
        padding: 0px;
        border: 0px;
    }

    @media screen and (min-width: ${({ theme }) => theme.screen.tablet}) {
    }
`;

export default {};
