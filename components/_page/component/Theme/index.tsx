import React from 'react';
import cx from 'classnames';
import { useSelector, useDispatch } from 'react-redux';
import { AppDispatch, RootState } from '@root/pages/_app';
import { SketchPicker } from 'react-color';
import { Icon, Drawer, ClickOutside } from '@scutoid/react';
import { useRouter } from 'next/router';
import { BodySTY, ListSTY } from './style';
import PaletteSVG from '@public/svg/Palette';

interface ThemeProps {
    keyValue?: string;
}
const Loading: React.FC<ThemeProps> = (props) => {
    const { keyValue } = props;
    const router = useRouter();

    const dispatch: AppDispatch = useDispatch();
    const { theme } = useSelector((state) => state) as RootState;
    const [isModalVisible, setIsModalVisible] = React.useState(false);

    const _setTheme = React.useCallback((theme) => dispatch({ type: 'SET_Theme', payload: theme }), [dispatch]);

    if (theme?.Scutoid?.hasOwnProperty(keyValue) === false) return null;
    return (
        <BodySTY className={cx('Theme')}>
            <Icon component={PaletteSVG} onClick={() => setIsModalVisible(true)} />
            <Drawer
                title={`Environmental Color - ${keyValue}`}
                visible={isModalVisible}
                onCancel={() => setIsModalVisible(false)}
            >
                <ListSTY>
                    {Object.keys(theme?.Scutoid[keyValue]).map((name, index) => {
                        return (
                            <ClickOutside key={`${name},${index}`}>
                                <ClickOutside.Trigger toggle>
                                    <div className="piker" title={name}>
                                        <div>{name}</div>
                                        <article>
                                            <sub style={{ backgroundColor: theme?.Scutoid[keyValue][name] }} />
                                            <strong>{theme?.Scutoid[keyValue][name]}</strong>
                                        </article>
                                    </div>
                                </ClickOutside.Trigger>
                                <ClickOutside.Window>
                                    <SketchPicker
                                        color={theme?.Scutoid[keyValue][name]}
                                        width="auto"
                                        onChangeComplete={(color) => {
                                            theme.Scutoid[keyValue][name] = color.hex;
                                            _setTheme(theme);
                                        }}
                                    />
                                </ClickOutside.Window>
                            </ClickOutside>
                        );
                    })}
                </ListSTY>
            </Drawer>
        </BodySTY>
    );
};

export default Loading;
