import React from 'react';
import cx from 'classnames';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { ListSTY } from './style';

interface ListProps {
    value?: string[];
    page?: string;
}
const List: React.FC<ListProps> = (props) => {
    const { page, value } = props;
    const router = useRouter();
    const [visible, setVisible] = React.useState(false);

    // React.useEffect(() => {
    //     /* 限制body捲動 */
    //     document.body.style.overflow = visible ? 'hidden' : '';
    // }, [visible]);

    return (
        <ListSTY
            className={cx(page, { hover: visible })}
            onMouseOver={() => setVisible(true)}
            onMouseEnter={() => setVisible(true)}
            onMouseLeave={() => setVisible(false)}
        >
            {value?.map((name) => (
                <Link key={`${page}-${name}`} href={{ pathname: `/${page}/[name]`, query: { name: name } }} passHref>
                    <a className={cx({ active: router?.query?.name === name })}>{name}</a>
                </Link>
            ))}
        </ListSTY>
    );
};
List.defaultProps = {
    page: 'components',
};
export default List;
