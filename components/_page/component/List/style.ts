import styled from 'styled-components';

export const ListSTY = styled.nav`
    display: grid;
    grid-gap: 8px;
    width: 200px;
    grid-auto-rows: min-content;
    border-right: 1px solid ${({ theme }) => theme.DefColor.SHIRONEZUMI};

    > a {
        color: ${({ theme }) => theme.DefColor.HAI};
        padding: 12px 4px 12px 12px;
        &:hover {
            color: ${({ theme }) => theme.DefColor.KIKYO};
        }
        &.active {
            color: ${({ theme }) => theme.DefColor.KIKYO};
            background-color: ${({ theme }) => theme.DefColor.OUCHI};
        }
    }

    @media screen and (max-width: ${({ theme }) => theme.screen.tablet}) {
        position: fixed;
        left: -200px;
        top: 0;
        bottom: 0;
        align-content: center;
        background-color: #fffffff0;
        z-index: 100;
        transition: left 0.3s;
        user-select: none;

        &.hover {
            left: 0;
        }

        &::after {
            content: '索引';
            position: absolute;
            top: 50%;
            left: 100%;
            transform: translateY(-50%);
            font-size: 20px;
            font-weight: bold;
            width: min-content;
            padding: 20px 12px;
            border-radius: 0 8px 8px 0;
            background-color: ${({ theme }) => theme.DefColor.OUCHI + 'aa'};
        }
    }
`;

export default {};
