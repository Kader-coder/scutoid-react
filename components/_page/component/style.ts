import styled from 'styled-components';

export const BodySTY = styled.div`
    position: relative;
    display: grid;
    overflow: hidden;
    border: 1px solid #eaeaea;
    border-radius: 2px;

    > div.children {
        padding: 20px 12px 12px;
        min-height: 120px;
    }

    .Divider {
        h3 {
            margin: 0px;
        }
    }

    article {
        font-size: smaller;
        padding: 8px;
        padding-left: 36px;
        color: #666;
    }

    > section {
        border-top: 1px dashed;
        border-color: inherit;

        &.Code {
            display: grid;
            line-height: 1.6;
            padding: 12px;
            letter-spacing: 1px;
            overflow-y: auto;

            &.hide {
                display: none;
            }

            > p {
                margin: 0;
                min-height: 12px;
                > span {
                    font-size: 15px;
                    &.keys,
                    &.html {
                        color: #e83015;
                    }
                    &.string {
                        color: #86c166;
                    }
                    &.props {
                        color: #ebb471;
                    }
                    &.main {
                        color: #51a8dd;
                    }
                }
            }
        }
    }
    @media screen and (min-width: ${({ theme }) => theme.screen.laptop}) {
    }
`;

export const ToolSTY = styled.section`
    padding: 8px;

    > div {
        display: grid;
        color: #999;
        width: fit-content;
        margin: auto;
    }

    button {
        border: 0px;
        padding: 0px;
        background-color: transparent;
        color: inherit;
        outline: none;
        cursor: pointer;
    }
    @media screen and (min-width: ${({ theme }) => theme.screen.laptop}) {
    }
`;

export default {};
