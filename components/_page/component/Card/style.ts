import styled, { css } from 'styled-components';

export const BodySTY = styled.nav`
    position: relative;
    display: grid;
    overflow: hidden;
    border: 1px solid #f0f0f0;
    border-radius: 2px;

    > img {
        min-height: 300px;
    }
`;

export default {};
