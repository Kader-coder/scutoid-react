import React from 'react';
import cx from 'classnames';
import Link from 'next/link';
import { BodySTY } from './style';

interface ListProps {
    name?: string;
}
const Card: React.FC<ListProps> = (props) => {
    const { name } = props;

    return (
        <Link href={{ pathname: '/components/[name]', query: { name: name } }} passHref>
            <BodySTY className={cx('Components')}>
                <img src="" alt={name} />
                <div>{name}</div>
            </BodySTY>
        </Link>
    );
};

export default Card;
