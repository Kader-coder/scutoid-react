import * as React from 'react';
import cx from 'classnames';
import Router from 'next/router';
import { BoxSTY } from './style';
// import Loading from '@components/Icon/Loading';
// import { useDispatch, useSelector } from 'react-redux';

interface T_Loading {
    loading: boolean;
    setLoading: React.Dispatch<React.SetStateAction<boolean>>;
    noLoading?: boolean;
}

const Module: React.FC<T_Loading> = (props) => {
    const { loading, setLoading, noLoading } = props;

    const _routeChangeStart = (e) => {
        const { pathname } = window.location;
        if (pathname !== e) {
            setLoading(true);
        }
    };

    // const _routeChangeComplete = () => {
    //     alert('_routeChangeComplete');
    // };
    React.useEffect(() => {
        Router.events.on('routeChangeStart', _routeChangeStart); // route Start
        // Router.events.on('routeChangeComplete', () => _routeChangeComplete); // route Change Complete
        noLoading && setLoading(false);

        // Router.beforePopState(() => {
        //     const result = window.confirm('are you sure you want to before page?');
        //     return result;
        // });

        return () => {
            Router.events.off('routeChangeStart', _routeChangeStart);
            // Router.events.off('routeChangeComplete', _routeChangeComplete);
        };
    }, []);

    React.useEffect(() => {
        document.body.style.overflow = loading ? 'hidden' : '';
    }, [loading]);

    // if (loading) return <BoxSTY />;
    return <BoxSTY className={cx({ loading: loading })} />;
};

export default Module;
