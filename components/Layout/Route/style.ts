import styled from 'styled-components';

export const BoxSTY = styled.div`
    position: fixed;
    display: flex;
    top: 0;
    left: 0;
    right: 0%;
    opacity: 0;
    height: 4px;
    z-index: 999;
    background-color: #9b90c2;
    user-select: none;
    transition: all 0.3s;

    &.loading {
        animation: example 5s ease;
        opacity: 1;
    }

    @keyframes example {
        0% {
            right: 100%;
        }
        100% {
            right: 0%;
        }
    }

    @media screen and (min-width: ${({ theme }) => theme.screen.laptop}) {
    }
`;

export default {};
