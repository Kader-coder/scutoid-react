import * as React from 'react';
import { useSelector } from 'react-redux';
import Head from 'next/head';
import { BodySTY } from './style';
import Route from './Route';
import BreadCrumb, { BreadCrumbType } from './BreadCrumb';

interface componentType {
    setLoading?: React.Dispatch<React.SetStateAction<boolean>>;
    setBreadCrumb?: React.Dispatch<React.SetStateAction<BreadCrumbType>>;
    data?: any;
}
export type ComponentType = React.FC<componentType>;

interface LayoutType {
    style?: React.CSSProperties;
    noLoading?: boolean;
}

const withLayout = (props: LayoutType) => (Component: ComponentType) => (morePros) => {
    const { style, noLoading } = props;
    const { setTheme } = morePros;
    const { theme } = useSelector((state) => state);
    const [load, setLoading] = React.useState(true);
    const [breadCrumb, setBreadCrumb] = React.useState<BreadCrumbType>(null);

    React.useEffect(() => {
        setTheme(setTheme);
    }, [theme]);

    return (
        <React.Fragment>
            <Head>
                <meta name="url" content={process.env.LOCAL} />
                <meta property="og:url" content={process.env.LOCAL} />
            </Head>
            <BodySTY className="Layout">
                <Route loading={load} setLoading={setLoading} noLoading={noLoading} />

                <main style={style}>
                    {!!breadCrumb && <BreadCrumb value={breadCrumb} />}
                    <Component setLoading={setLoading} setBreadCrumb={setBreadCrumb} data={morePros} />
                </main>
            </BodySTY>
        </React.Fragment>
    );
};

export default withLayout;
