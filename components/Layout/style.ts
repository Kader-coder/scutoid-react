import styled from 'styled-components';

export const BodySTY = styled.div`
    > main {
        display: grid;
        width: 100%;
        max-width: 100%;
        margin: 0 auto;
        padding: 12px 10px 24px;
        > div {
            overflow: auto;
        }
    }

    @media screen and (min-width: ${({ theme }) => theme.screen.laptop}) {
        > main {
            max-width: 1000px;
        }
    }

    @media screen and (min-width: ${({ theme }) => theme.screen.desktop}) {
        > main {
            max-width: 1280px;
        }
    }
`;

export default {};
