import * as React from 'react';
import cx from 'classnames';
import Link from 'next/link';
import Meun from '@components/Icon/Meun';
import { useRouter } from 'next/router';
import { Fixed } from '@scutoid/react';
import { BodySTY, ToolSTY } from './style';
import _ from 'lodash';

interface R_Header {
    fixed?: boolean;
}
const list = {
    Components: {
        pathname: '/components',
    },
    Docs: {
        pathname: '/docs/Start',
    },
    // About: '#about',
    Home: {
        pathname: '/',
    },
};

const Header: React.FC<R_Header> = (props) => {
    const { fixed } = props;
    const router = useRouter();
    const [state, setstate] = React.useState(false);
    const Module_ref = React.useRef<HTMLElement>(null);

    const _handleClickOutside = (event: Event) => {
        if (Module_ref.current && !Module_ref.current.contains(event.target as Node)) {
            setstate(false);
        }
    };

    const _Scroll = () => {
        setstate(false);
    };

    React.useEffect(() => {
        setstate(false);
    }, [router.query]);

    React.useEffect(() => {
        document.addEventListener('mousedown', _handleClickOutside);
        document.addEventListener('scroll', _Scroll);
        return () => {
            document.removeEventListener('mousedown', _handleClickOutside);
            document.removeEventListener('scroll', _Scroll);
        };
    }, []);

    return (
        <BodySTY ref={Module_ref}>
            <h1>Scutoid</h1>
            <div></div>
            <a href={`${process.env.NPM_HERF}`}>V 0.2.2</a>
            <ToolSTY className={cx({ open: state })}>
                {Object.keys(list).map((name) => (
                    <Link key={`Header-${name}`} href={list[name]} passHref>
                        <a className={cx({ active: router?.asPath?.includes(list[name].pathname) })}>{name}</a>
                    </Link>
                ))}
            </ToolSTY>
            <Meun value={state} onClick={() => setstate((pre) => !pre)} />
        </BodySTY>
    );
};

Header.defaultProps = {
    fixed: true,
};

export default Header;
