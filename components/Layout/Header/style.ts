import styled from 'styled-components';

export const BodySTY = styled.header`
    display: grid;
    left: 0;
    right: 0;
    grid-template-columns: auto 1fr auto auto;
    grid-gap: 4px;
    align-items: center;
    padding: 8px calc((100% - 980px) / 2);
    box-shadow: 0 3px 8px #f0f0f0;
    background-color: ${({ theme }) => theme.DefColor.GOFUN};
    z-index: 10;
    user-select: none;

    > h1 {
        margin: 0px;
        padding: 0px 12px;
        font-size: 24px;
        font-weight: bold;
        font-family: monospace;
        letter-spacing: 2px;
    }
    a {
        color: ${({ theme }) => theme.DefColor.HAI};
        padding: 8px;
        &:hover {
            color: ${({ theme }) => theme.DefColor.KESHIZUMI};
        }
    }
    @media screen and (min-width: ${({ theme }) => theme.screen.tablet}) {
    }

    @media screen and (min-width: ${({ theme }) => theme.screen.desktop}) {
        padding: 8px calc((100% - 1260px) / 2);
    }
`;

export const ToolSTY = styled.nav`
    display: grid;
    grid-auto-flow: column;
    grid-auto-columns: max-content;
    grid-gap: 16px;
    background-color: inherit;

    > a {
        line-height: 1;
        cursor: pointer;
        &:hover {
            filter: brightness(0.5);
        }
        &.active {
            color: ${({ theme }) => theme.DefColor.KIKYO};
        }
    }

    @media screen and (max-width: ${({ theme }) => theme.screen.tablet}) {
        position: absolute;
        top: 56px;
        left: 0;
        right: 0;
        grid-auto-flow: row;
        padding: 16px;
        font-size: 16px;
        align-content: center;
        justify-content: center;
        justify-items: center;
        transform-origin: top center;
        box-shadow: inherit;
        transform: scaley(0);
        transition: transform 0.26s;

        &.open {
            transform: scaley(1);
        }
    }
`;

export default {};
