import styled from 'styled-components';

export const BodySTY = styled.footer`
    display: grid;
    grid-gap: 16px;
    padding: 24px calc((100% - 980px) / 2);
    background-color: ${({ theme }) => theme.DefColor.SUMI};
    color: ${({ theme }) => theme.DefColor.SHIRONERI};

    > address,
    > nav {
        padding: 0 10px;
    }

    > nav {
        display: grid;
        grid-auto-flow: column;
        grid-gap: 32px;
        width: fit-content;

        > div {
            display: grid;
            grid-gap: 4px;

            > p {
                margin: 0px 0px 8px;
                font-size: 18px;

                font-weight: bold;
                letter-spacing: 1px;
            }
            > a {
                color: #f0f0f0;
                font-size: 14px;
                &:hover {
                    text-decoration: underline;
                }
            }
        }
    }

    > address {
        p {
            margin: 0px;
            font-size: 24px;
            font-weight: bold;
            font-family: monospace;
            letter-spacing: 2px;
        }
        div {
            font-size: 11px;
            color: ${({ theme }) => theme.DefColor.SHIRONEZUMI};
        }
    }

    @media screen and (min-width: ${({ theme }) => theme.screen.laptop}) {
    }
    @media screen and (min-width: ${({ theme }) => theme.screen.desktop}) {
        padding: 8px calc((100% - 1260px) / 2);
    }
`;

export default {};
