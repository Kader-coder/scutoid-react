import * as React from 'react';
import Link from 'next/link';
import { BodySTY } from './style';

const list = [
    {
        name: 'General resources',
        children: [
            { name: 'Components', herf: '/components' },
            { name: 'Docs', herf: '/docs' },
        ],
    },
    {
        name: 'About',
        children: [
            { name: 'Contact ', herf: 'mailto:app030242@gmail.com' },
            { name: 'GitLab', herf: 'https://gitlab.com/Kader-coder' },
        ],
    },
];

const Footer: React.FC = () => {
    return (
        <BodySTY id="about">
            <nav>
                {list.map((item, index) => {
                    return (
                        <div key={`${item.name}-${index}`}>
                            <p>{item.name}</p>
                            {item.children.map((children, index) => {
                                return (
                                    <Link key={`${children.name}-${index}`} href={children.herf} passHref>
                                        <a>{children.name}</a>
                                    </Link>
                                );
                            })}
                            <a></a>
                        </div>
                    );
                })}
            </nav>
            <address>
                <p>Scutoid</p>
                <div>Copyright © 2021 ChengYu</div>
            </address>
        </BodySTY>
    );
};

export default Footer;
