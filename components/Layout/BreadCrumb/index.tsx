import React from 'react';
import cx from 'classnames';
import Link, { LinkProps } from 'next/link';
import { BodySTY } from './style';

export type BreadCrumbType = { name: string; link?: React.PropsWithChildren<LinkProps> }[];

interface BreadCrumb_T {
    value?: BreadCrumbType;
}
const init: BreadCrumbType = [
    {
        name: '首頁',
        link: {
            href: {
                pathname: '/',
            },
        },
    },
];
const BreadCrumbMoudle: React.FC<BreadCrumb_T> = (props) => {
    const { value } = props;

    const [breadCrumb, setBreadCrumb] = React.useState<typeof value>(null);

    React.useEffect(() => {
        if (value) {
            setBreadCrumb(init.concat(value));
        }
    }, [value]);

    return (
        <BodySTY className="BreadCrumb">
            {breadCrumb?.map((item, index) => {
                if (item?.link) {
                    return (
                        <Link key={`BreadCrumb${index}`} {...item.link} passHref>
                            <a>{item?.name}</a>
                        </Link>
                    );
                } else {
                    return <a key={`BreadCrumb${index}`}>{item?.name}</a>;
                }
            })}
        </BodySTY>
    );
};

// <Link {...link} passHref>
export default BreadCrumbMoudle;
