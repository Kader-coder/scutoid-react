import styled from 'styled-components';

export const BodySTY = styled.nav`
    display: grid;
    grid-auto-flow: column;
    grid-auto-columns: max-content;
    grid-gap: 4px;
    max-width: 1200px;
    width: 100%;
    color: ${({ theme }) => theme.DefColor.Dark};
    margin: 8px auto;

    > a {
        &:not([href]) {
            color: ${({ theme }) => theme.DefColor.Silver};
            cursor: not-allowed;
        }
        &:nth-child(n + 2)::before {
            content: '>';
            color: ${({ theme }) => theme.DefColor.Dark};
        }
    }
    @media screen and (min-width: ${({ theme }) => theme.screen.laptop}) {
    }
`;

export default {};
