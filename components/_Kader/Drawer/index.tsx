import { Button, Radio, Drawer } from '@scutoid/react';

import * as React from 'react';

export default {
    title: 'Feedback(反饋類)/Drawer',
    component: Drawer,
};

export const Common = () => {
    const [isModalVisible, setIsModalVisible] = React.useState(false);

    return (
        <div>
            <Button onClick={() => setIsModalVisible(true)}>OPEN</Button>
            <Drawer title="Option" visible={isModalVisible} onCancel={() => setIsModalVisible(false)}>
                <div>Info.....</div>
                <div>Info.....</div>
                <div>Info.....</div>
            </Drawer>
        </div>
    );
};

export const Placement = () => {
    const [isModalVisible, setIsModalVisible] = React.useState(false);
    const [placement, setPlacement] = React.useState<'top' | 'right' | 'bottom' | 'left'>('right');

    return (
        <div>
            <Radio.Group
                value={placement}
                onChange={(v) => {
                    setPlacement(v as typeof placement);
                }}
            >
                <Radio value={'right'}>right</Radio>
                <Radio value={'left'}>left</Radio>
                <Radio value={'top'}>top</Radio>
                <Radio value={'bottom'}>bottom</Radio>
            </Radio.Group>

            <Button onClick={() => setIsModalVisible(true)}>OPEN</Button>

            <Drawer
                title="Option"
                placement={placement}
                visible={isModalVisible}
                onCancel={() => setIsModalVisible(false)}
            >
                <div>Info.....</div>
                <div>Info.....</div>
                <div>Info.....</div>
            </Drawer>
        </div>
    );
};
