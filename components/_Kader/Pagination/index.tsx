import { Pagination } from '@scutoid/react';
import * as React from 'react';

export default {
    title: 'Common(通用類)/Pagination',
    component: Pagination,
};

export const Common = () => {
    return (
        <div>
            <Pagination total={50} page={4} onChange={(e) => console.log(e)} />
        </div>
    );
};
