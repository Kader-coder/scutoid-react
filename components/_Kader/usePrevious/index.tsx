import { usePrevious, Button } from '@scutoid/react';
import * as React from 'react';

export default {
    title: 'Common(通用類)/usePrevious',
    component: usePrevious,
};

export const Common = () => {
    const [state, setstate] = React.useState(0);
    const previous = usePrevious(state);

    return (
        <div style={{ display: 'grid', gridGap: '12px' }}>
            <div>
                previous:{previous} state:{state}
            </div>
            <Button onClick={() => setstate((pre) => pre + 1)}>Contained</Button>
        </div>
    );
};
