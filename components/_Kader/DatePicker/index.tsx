import { DatePicker } from '@scutoid/react';
import * as React from 'react';
import dayjs from 'dayjs';

export default {
    title: 'DataInput(資料輸入類)/DatePicker',
    component: DatePicker,
};

export const Common = () => {
    return (
        <div>
            <div
                style={{
                    display: 'flex',
                    flexFlow: 'wrap',
                    columnGap: '24px',
                    marginBottom: '10px',
                }}
            >
                <DatePicker onChange={(v) => console.log(v)} />
                <DatePicker defaultValue="2020" onChange={(v) => console.log(v)} />
                <DatePicker defaultValue="202012" onChange={(v) => console.log(v)} />
            </div>
            <div
                style={{
                    display: 'flex',
                    flexFlow: 'wrap',
                    columnGap: '24px',
                }}
            >
                <DatePicker defaultValue="20201231" onChange={(v) => console.log(v)} />
                <DatePicker defaultValue="20201231" format="YYYY/MM/DD" onChange={(v) => console.log(v)} />
            </div>
        </div>
    );
};

export const DisabledDate = () => {
    const [data, setValue] = React.useState('');

    React.useEffect(() => {
        setValue('20201231');
    }, []);

    return (
        <div
            style={{
                display: 'grid',
                gridGap: '24px',
            }}
        >
            <DatePicker defaultValue={data} onChange={(v) => console.log(v)} />
            <DatePicker disabledDate={(v) => dayjs().isBefore(dayjs(v))} onChange={(v) => console.log(v)} />
        </div>
    );
};

export const Other = () => {
    return (
        <div
            style={{
                display: 'grid',
                gridGap: '24px',
            }}
        >
            <DatePicker placeholder="Hello" onChange={(v) => console.log(v)} />
            <DatePicker disabled placeholder="Hello" onChange={(v) => console.log(v)} />
        </div>
    );
};
