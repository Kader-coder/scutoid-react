import { Divider } from '@scutoid/react';
import * as React from 'react';

export default {
    title: 'Common(通用類)/Divider',
    component: Divider,
};

export const Common = () => {
    return (
        <div>
            <Divider>HelloWorld</Divider>
        </div>
    );
};

export const Orientation = () => {
    return (
        <div>
            <Divider orientation="left">Left</Divider>
            <Divider orientation="right">Right</Divider>
        </div>
    );
};

export const vertical = () => {
    return (
        <div>
            <Divider vertical>Vertical</Divider>
        </div>
    );
};

export const reserve = () => {
    return (
        <div>
            <Divider orientation="left" reserve="100px">
                Left 100px
            </Divider>
            <Divider orientation="left" reserve="200px">
                Left 200px
            </Divider>
            <Divider orientation="left" reserve="300px">
                Left 300px
            </Divider>
            <Divider orientation="left" reserve="400px">
                Left 400px
            </Divider>
        </div>
    );
};
