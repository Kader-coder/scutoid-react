import { Tag } from '@scutoid/react';
import * as React from 'react';

export default {
    title: 'DataDisplay(資料顯示類)/Tag',
    component: Tag,
};

export const common = () => {
    return (
        <div style={{ margin: '10px' }}>
            <Tag>Common</Tag>
            <Tag>
                <a href="https://www.google.com/">Link</a>
            </Tag>
            <Tag
                closable
                onClose={() => {
                    console.log('onClose !');
                }}
            >
                onClose
            </Tag>
        </div>
    );
};

export const ColorTag = () => {
    return (
        <div style={{ margin: '10px' }}>
            <Tag
                color="red"
                fill
                onClose={() => {
                    console.log('onClose !');
                }}
            >
                red
            </Tag>
            <Tag color="blue">
                <a href="https://www.google.com/">Link</a>
            </Tag>
            <Tag color="orange">Taorangeg</Tag>
            <Tag color="green">green</Tag>
            <Tag color="purple">purple</Tag>
            <Tag color="gold">gold</Tag>
            <Tag color="cadetblue">cadetblue</Tag>
            <Tag color="#123456">#123456</Tag>
            <Tag color="#654321">#654321</Tag>
            <Tag color="#789abc">#789abc</Tag>
            <Tag color="#000">#000</Tag>
        </div>
    );
};
