import { Input } from '@scutoid/react';
import * as React from 'react';

export default {
    title: 'Common(通用類)/Input',
    component: Input,
};

export const Common = () => {
    const [state, setstate] = React.useState({ name: '123' });
    return (
        <div
            style={{
                display: 'grid',
                gridGap: '24px',
            }}
        >
            <Input title={'Hello'} value={state.name} placeholder={'Input'} character={10} rule="number" />
            <Input placeholder={'Input'} character={10} rule="number" />
            <Input required title={'Name'} type="password" />
            <Input mod="Without" value={'Input'} />
            <button
                onClick={() =>
                    setstate((pre) => {
                        pre.name = '444ddd';
                        return { ...pre };
                    })
                }
            >
                setValue
            </button>
        </div>
    );
};

export const Textarea = () => {
    return (
        <div>
            <Input as="textarea" title={'Hello'} placeholder={'Input'} character={100} />
        </div>
    );
};
