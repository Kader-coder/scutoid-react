import { Button, Dialog } from '@scutoid/react';
import * as React from 'react';

export default {
    title: 'Feedback(反饋類)/Dialog',
    component: Dialog,
};

export const Info = () => {
    const [isModalVisible, setIsModalVisible] = React.useState(false);

    return (
        <div>
            <Button onClick={() => setIsModalVisible(true)}>OPEN</Button>
            <Dialog title="GGGGGGGG" visible={isModalVisible} onCancel={() => setIsModalVisible(false)} onOK={() => console.log('onOK')}>
                <div>Info.....</div>
                <div>Info.....</div>
                <div>Info.....</div>
            </Dialog>
        </div>
    );
};
