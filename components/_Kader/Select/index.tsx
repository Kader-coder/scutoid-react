import * as React from 'react';
import { Select } from '@scutoid/react';

export default {
    title: 'DataInput(資料輸入類)/Select',
    component: Select,
};

export const Common = () => {
    return (
        <div>
            <Select
                defValue={1}
                data={[
                    { text: 'apple', value: 1 },
                    { text: 'banana', value: 2 },
                    { text: 'gogogo', value: 3 },
                ]}
                onChange={(value) => console.log(Number(value))}
            />
        </div>
    );
};

export const AllSelect = () => {
    return (
        <div>
            <Select
                data={[
                    { text: 'apple', value: 1 },
                    { text: 'banana', value: 2 },
                    { text: 'gogogo', value: 3 },
                ]}
                onChange={(value) => console.log(Number(value))}
                all
            />
        </div>
    );
};
