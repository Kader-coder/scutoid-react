import { Button, Windows } from '@scutoid/react';

import * as React from 'react';

export default {
    title: 'Feedback(反饋類)/Windows',
    component: Windows,
};

export const Info = () => {
    const [isModalVisible, setIsModalVisible] = React.useState(false);

    return (
        <div>
            <Button onClick={() => setIsModalVisible(true)}>OPEN</Button>
            <Windows visible={isModalVisible} onCancel={() => setIsModalVisible(false)}>
                <div>
                    <div>Info.....</div>
                    <div>Info.....</div>
                    <div>Info.....</div>
                </div>
                <div>Info.....</div>
                <div>Info.....</div>
                <div>Info.....</div>
            </Windows>
        </div>
    );
};
