import { Table } from '@scutoid/react';
import * as React from 'react';

export default {
    title: 'DataDisplay(資料顯示類)/Table',
    component: Table,
};

export const common = () => {
    const data = [
        { key: 1, location: 'Tokyo', temp: 20, weather: 1, pop: 20 },
        { key: 2, location: 'Seoul', temp: 23, weather: 3, pop: 80 },
        { key: 3, location: 'Taipei', temp: 24, weather: 1, pop: 0 },
        { key: 4, location: 'Singapore', temp: 24, weather: 1, pop: 10 },
        { key: 5, location: 'Manila', temp: 24, weather: 2, pop: 50 },
    ];
    return (
        <div style={{ margin: '10px' }}>
            <Table
                columns={[
                    {
                        title: '地點',
                        dataIndex: 'location',
                    },
                    {
                        title: '溫度',
                        dataIndex: 'temp',
                        sort: (a, b) => a > b,
                    },
                    {
                        title: '天氣',
                        dataIndex: 'weather',
                    },
                    {
                        title: '降雨機率(%)',
                        dataIndex: 'pop',
                        sort: (a, b) => a > b,
                        render: (v) => `${v}%`,
                    },
                ]}
                dataSource={data}
            />
        </div>
    );
};

export const checkbox = () => {
    const data = [
        { key: 1, location: 'Tokyo', temp: 20, weather: 1, pop: 20 },
        { key: 2, location: 'Seoul', temp: 23, weather: 3, pop: 80 },
        { key: 3, location: 'Taipei', temp: 24, weather: 1, pop: 0 },
        { key: 4, location: 'Singapore', temp: 24, weather: 1, pop: 10 },
        { key: 5, location: 'Manila', temp: 24, weather: 2, pop: 50 },
    ];

    return (
        <div style={{ margin: '10px' }}>
            <Table
                rowSelection={{ type: 'checkbox', onChange: (v) => console.log(v) }}
                columns={[
                    {
                        title: '地點',
                        dataIndex: 'location',
                    },
                    {
                        title: '溫度',
                        dataIndex: 'temp',
                        sort: (a, b) => a > b,
                    },
                    {
                        title: '天氣',
                        dataIndex: 'weather',
                    },
                    {
                        title: '降雨機率(%)',
                        dataIndex: 'pop',
                        sort: (a, b) => a > b,
                        render: (v) => `${v}%`,
                    },
                ]}
                dataSource={data}
            />
        </div>
    );
};

export const radio = () => {
    const data = [
        { key: 1, location: 'Tokyo', temp: 20, weather: 1, pop: 20 },
        { key: 2, location: 'Seoul', temp: 23, weather: 3, pop: 80 },
        { key: 3, location: 'Taipei', temp: 24, weather: 1, pop: 0 },
        { key: 4, location: 'Singapore', temp: 24, weather: 1, pop: 10 },
        { key: 5, location: 'Manila', temp: 24, weather: 2, pop: 50 },
    ];

    return (
        <div style={{ margin: '10px' }}>
            <Table
                rowSelection={{ type: 'radio', onChange: (v) => console.log(v) }}
                columns={[
                    {
                        title: '地點',
                        dataIndex: 'location',
                    },
                    {
                        title: '溫度',
                        dataIndex: 'temp',
                        sort: (a, b) => a > b,
                    },
                    {
                        title: '天氣',
                        dataIndex: 'weather',
                    },
                    {
                        title: '降雨機率(%)',
                        dataIndex: 'pop',
                        sort: (a, b) => a > b,
                        render: (v) => `${v}%`,
                    },
                ]}
                dataSource={data}
            />
        </div>
    );
};
