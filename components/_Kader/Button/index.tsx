import { Button } from '@scutoid/react';
import * as React from 'react';

export default {
    title: 'Common(通用類)/Button',
    component: Button,
};

export const Common = () => {
    return (
        <div style={{ display: 'flex' }}>
            <Button type="Contained">Contained</Button>
            <Button type="Text">Text</Button>
            <Button type="Outlined">Outlined</Button>
        </div>
    );
};
