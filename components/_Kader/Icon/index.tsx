import * as React from 'react';
import styled from 'styled-components';
import { Icon, IconType } from '@scutoid/react';

export default {
    title: 'Common(通用類)/Icon',
    component: Icon,
};

const BodySTY = styled.div`
    display: grid;
    grid-template-columns: repeat(4, 1fr);
    grid-gap: 12px;
    > div {
        text-align: center;
        padding: 8px;
        border-radius: 6px;
        cursor: pointer;

        label {
            display: block;
            margin-top: 4px;
        }
        &:hover {
            background: ${({ theme }) => theme.DefColor.HAI};
            color: #fff;
        }
    }
`;

export const IconList = () => {
    return (
        <BodySTY>
            {IconType.map((name) => {
                return (
                    <div className="item" key={name}>
                        <Icon type={name} />
                        <label>{name}</label>
                    </div>
                );
            })}
        </BodySTY>
    );
};

export const ColorSize = () => {
    return (
        <div>
            <Icon type="heart" color="#F00" />
            <Icon type="heart" color="#0F0" fontSize="40" />
            <Icon type="heart" color="#00F" fontSize="60" />
        </div>
    );
};
