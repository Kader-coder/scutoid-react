import { Tab } from '@scutoid/react';
import * as React from 'react';

export default {
    title: 'navigate(頁面導航類)/Tab',
    component: Tab,
};

export const Common = () => {
    return (
        <div>
            <Tab defaultValue={1}>
                <Tab.TabPane name={'Apple'} keys={1}>
                    Apple Inc. is an American multinational technology company headquartered in Cupertino, California,
                    that designs, develops, and sells consumer electronics, computer software, and online services. It
                    is considered one of the Big Five companies in the U.S. information technology industry, along with
                    Amazon, Google, Microsoft, and Facebook. Its hardware products include the iPhone smartphone, the
                    iPad tablet computer, the Mac personal computer, the iPod portable media player, the Apple Watch
                    smartwatch, the Apple TV digital media player, the AirPods wireless earbuds, the AirPods Max
                    headphones, and the HomePod smart speaker line.
                </Tab.TabPane>
                <Tab.TabPane name={'Facebook'} keys={2}>
                    Facebook (stylized as facebook) is an American online social media and social networking service
                    based in Menlo Park, California, and a flagship service of the namesake company Facebook, Inc. It
                    was founded by Mark Zuckerberg, along with fellow Harvard College students and roommates Eduardo
                    Saverin, Andrew McCollum, Dustin Moskovitz, and Chris Hughes.
                </Tab.TabPane>
                <Tab.TabPane name={'Twitter'} keys={3}>
                    Twitter is an American microblogging and social networking service on which users post and interact
                    with messages known as tweets. Registered users can post, like and retweet tweets, but unregistered
                    users can only read them. Users access Twitter through its website interface or its mobile-device
                    application software, though the service could also be accessed via SMS before April 2020.[13]
                    Twitter, Inc. is based in San Francisco, California, and has more than 25 offices around the world.
                    Tweets were originally restricted to 140 characters, but was doubled to 280 for non-CJK languages in
                    November 2017. Audio and video tweets remain limited to 140 seconds for most accounts.
                </Tab.TabPane>
            </Tab>
        </div>
    );
};
