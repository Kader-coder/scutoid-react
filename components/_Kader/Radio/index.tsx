import { Radio } from '@scutoid/react';
import * as React from 'react';

export default {
    title: 'Common(通用類)/Radio',
    component: Radio,
};

export const Common = () => {
    return (
        <div style={{ display: 'grid', gridGap: 8 }}>
            <Radio>Common</Radio>
            <Radio disabled>(disabled)</Radio>
        </div>
    );
};

export const Group = () => {
    const [radiovalue, setRadioValue] = React.useState<any>(4);

    return (
        <Radio.Group
            value={radiovalue}
            onChange={(v) => {
                setRadioValue(v);
                console.log(v);
            }}
            // disabled
        >
            <Radio value={1}>Apple</Radio>
            <Radio value={2}>Banana</Radio>
            <Radio value={3}>AK47</Radio>
            <Radio value={4} disabled>
                disabled
            </Radio>
            <button onClick={() => setRadioValue(null)}>reset</button>
        </Radio.Group>
    );
};

export const Button = () => {
    const [radiovalue, setRadioValue] = React.useState<any>(4);

    return (
        <Radio.Group
            value={radiovalue}
            onChange={(v) => {
                console.log(v);
                setRadioValue(v);
            }}
            // disabled
        >
            <Radio.Button value={1}>TSM</Radio.Button>
            <Radio.Button value={2}>AMD</Radio.Button>
            <Radio.Button value={3}>AAPL</Radio.Button>
            <Radio.Button value={4}>ED</Radio.Button>
        </Radio.Group>
    );
};
