import { Checkbox, Input } from '@scutoid/react';
import * as React from 'react';

export default {
    title: 'Common(通用類)/Checkbox',
    component: Checkbox,
};

export const Common = () => {
    return (
        <div style={{ display: 'grid', gridGap: 8 }}>
            <Checkbox>Apple</Checkbox>
            <Checkbox defaultChecked>Banana</Checkbox>
            <Checkbox checked>AKB</Checkbox>
            <Checkbox disabled>Disabled</Checkbox>
        </div>
    );
};

export const Group = () => {
    const [radiovalue, setCheckboxValue] = React.useState<string[]>(['4']);

    return (
        <Checkbox.Group
            value={radiovalue}
            onChange={(v) => {
                if (v.length > 5) {
                    console.log('超過2個');
                } else {
                    setCheckboxValue(v);
                }
            }}
        >
            <Checkbox value={'1'}>Apple</Checkbox>
            <Checkbox value={'2'}>Banana</Checkbox>
            <Checkbox value={'3'}>AR47</Checkbox>
            <Checkbox value={'4'} disabled>
                Disabled
            </Checkbox>
            <button onClick={() => setCheckboxValue([])}>reset</button>
        </Checkbox.Group>
    );
};

export const Size = () => {
    const [size, setSize] = React.useState(14);

    return (
        <div style={{ display: 'grid', gridGap: 8 }}>
            <Input title="size" rule="number" required value={size || ''} onChange={(e) => setSize(Number(e.target.value))} />
            <Checkbox size={size} defaultChecked>
                {size}px
            </Checkbox>
            <Checkbox size={size}>{size}px</Checkbox>
        </div>
    );
};
