require('dotenv').config();

const webpack = require('webpack');
const path = require('path');
const root = path.resolve('./');
const resolveTsconfigPathsToAlias = require('./utils/resolveTsconfigPathsToAlias.ts');

const fs = require('fs');
const list = fs.readdirSync(path.resolve(root, './components/_Kader'));

const components = {};
list.forEach((folder) => {
    fs.readFile(path.resolve(root, `./components/_Kader/${folder}/index.tsx`), function (err, data) {
        if (err) throw err;
        components[folder] = data.toString();
    });
});

const child_process = require('child_process');
function git(command) {
    return child_process.execSync(`git ${command}`, { encoding: 'utf8' }).trim();
}

module.exports = {
    module: {},
    node: {
        fs: 'empty',
    },
    resolve: {
        alias: resolveTsconfigPathsToAlias({
            tsconfigPath: path.resolve(root, 'tsconfig.json'),
            webpackConfigBasePath: './',
        }),
    },
    // plugins: [
    //     new webpack.DefinePlugin({
    //         'process.env': {
    //             DEBUG: process.env.DEBUG == 'true' ? true : false,
    //         },
    //     }),
    // ],
    plugins: [
        new webpack.EnvironmentPlugin({
            GIT_VERSION: git('describe --always'),
            GIT_AUTHOR_DATE: git('log -1 --format=%aI'),
            LIST: list,
            Components: components,
        }),
    ],
};
