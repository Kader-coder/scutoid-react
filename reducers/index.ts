import { combineReducers } from 'redux';
import theme from './theme';
import visibilityFilter from './visibilityFilter';

const reducers = {
    theme,
    visibilityFilter,
};

const todoApp = combineReducers(reducers);

export default todoApp;
