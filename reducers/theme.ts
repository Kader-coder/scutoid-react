import theme from '@style/theme';
let Theme = theme;

const _Theme = (state = theme, action) => {
    switch (action.type) {
        case 'SET_Theme':
            Theme = { ...theme };
            return Theme;
        default:
            return state;
    }
};

export default _Theme;
